package com.example.pba_lab3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PbaLab3Application {

	public static void main(String[] args) {
		SpringApplication.run(PbaLab3Application.class, args);
	}

}
